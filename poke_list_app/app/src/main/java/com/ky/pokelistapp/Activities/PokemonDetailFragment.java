package com.ky.pokelistapp.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.ky.pokelistapp.Adapter.PokemonTypeAdapter;
import com.ky.pokelistapp.Constant;
import com.ky.pokelistapp.Db.DbHelper;
import com.ky.pokelistapp.Model.EvolSimple;
import com.ky.pokelistapp.Model.Pokemon;
import com.ky.pokelistapp.Utils.Common;
import com.ky.pokelistapp.databinding.FragmentPokemonMoreDetailBinding;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class PokemonDetailFragment extends Fragment {

    static PokemonDetailFragment instance;
    FragmentPokemonMoreDetailBinding moreDetBinding;
    Context mContext;
    DbHelper dbHelper;

    public static PokemonDetailFragment getInstance(){
        if(instance == null){
            instance = new PokemonDetailFragment();
        }

        return  instance;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){
            mContext = context;
        }
    }

    public PokemonDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DbHelper(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        moreDetBinding = FragmentPokemonMoreDetailBinding.inflate(inflater, container, false);

        // get data
        Pokemon pokemon = dbHelper.getPokemon(getArguments().getString(Constant.KEY_NUM));

        setupRvViews();
        setData(pokemon);

        return moreDetBinding.getRoot();
    }

    private void setupRvViews(){
        moreDetBinding.rvType.setHasFixedSize(true);
        moreDetBinding.rvType.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        moreDetBinding.rvWeakness.setHasFixedSize(true);
        moreDetBinding.rvWeakness.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

    }

    private void setData(Pokemon pokemon){

        Glide.with(mContext).load(pokemon.getImg()).into(moreDetBinding.ivPokemoneImage);
        moreDetBinding.tvName.setText(pokemon.getName());
        moreDetBinding.tvWeight.setText(pokemon.getWeight());
        moreDetBinding.tvHeight.setText(pokemon.getName());
        moreDetBinding.tvCandy.setText(pokemon.getCandy());
        moreDetBinding.tvCandyCount.setText(String.valueOf(pokemon.getCandy_count()));
        moreDetBinding.tvEgg.setText(pokemon.getEgg());

        PokemonTypeAdapter typeAdapter = new PokemonTypeAdapter(mContext, pokemon.getType());
        moreDetBinding.rvType.setAdapter(typeAdapter);

        PokemonTypeAdapter weaknessAdapter = new PokemonTypeAdapter(mContext, pokemon.getWeaknesses());
        moreDetBinding.rvWeakness.setAdapter(weaknessAdapter);

        if(!pokemon.getPrev_evol_num().isEmpty()) {
            EvolSimple prevEvol = dbHelper.getEvol(pokemon.getPrev_evol_num());
            Glide.with(mContext).load(prevEvol.getImg()).into(moreDetBinding.ivPrevEvol);

            moreDetBinding.ivPrevEvol.setOnClickListener(view -> LocalBroadcastManager.getInstance(mContext)
                    .sendBroadcast(new Intent(Common.KEY_NUM_EVOLUTION).putExtra(Constant.KEY_NUM, prevEvol.getNum())));
        }

        if(!pokemon.getNext_evol_num().isEmpty()) {
            EvolSimple nextEvol = dbHelper.getEvol(pokemon.getNext_evol_num());
            Glide.with(mContext).load(nextEvol.getImg()).into(moreDetBinding.ivNextEvol);

            moreDetBinding.ivNextEvol.setOnClickListener(view -> LocalBroadcastManager.getInstance(mContext)
                    .sendBroadcast(new Intent(Common.KEY_NUM_EVOLUTION).putExtra(Constant.KEY_NUM, nextEvol.getNum())));
        }
    }
}