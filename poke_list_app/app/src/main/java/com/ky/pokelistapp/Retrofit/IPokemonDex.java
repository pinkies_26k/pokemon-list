package com.ky.pokelistapp.Retrofit;

import com.ky.pokelistapp.Model.Pokedex;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;

public interface IPokemonDex {

    @GET("pokedex.json")
    Observable<Pokedex> getListPokemon();

    @GET("pokedex.json")
    Call<Pokedex> getListPokemonByCall();
}
