package com.ky.pokelistapp.Model;

public class EvolSimple {
    public String num;
    public String name;
    private String img;

    public EvolSimple(String num, String name, String pic) {
        this.num = num;
        this.name = name;
        this.img = pic;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
