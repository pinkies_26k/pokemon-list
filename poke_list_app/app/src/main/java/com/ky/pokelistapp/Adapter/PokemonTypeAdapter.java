package com.ky.pokelistapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ky.pokelistapp.Interface.IItemClickListener;
import com.ky.pokelistapp.Utils.Common;
import com.ky.pokelistapp.databinding.ItemChipBinding;

import java.util.List;

public class PokemonTypeAdapter extends RecyclerView.Adapter<PokemonTypeAdapter.MyViewHolder> {

    Context context;
    List<String> typeList;

    public PokemonTypeAdapter(Context context, List<String> typeList) {
        this.context = context;
        this.typeList = typeList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemChipBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        String type = typeList.get(position);
        holder.binding.chip.setText(type);
        holder.binding.chip.setChipBackgroundColor(Common.getColorByType(type));
    }

    @Override
    public int getItemCount() {
        return typeList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ItemChipBinding binding;
        IItemClickListener iItemClickListener;

        public MyViewHolder(ItemChipBinding itemChipBinding) {
            super(itemChipBinding.getRoot());
            this.binding = itemChipBinding;

        }

    }
}
