# Pokedex Clone (1st Gen) Mobile App
Pokedex clone (1st Gen) app is a sample Pokedex application that shows a list of 1st generation Pokemon (151 Pokemon in total).

## Libraries / Dependencies
Uses the following libraries : 

- Retrofit  (Fetch data) 
- Gson (Working with Json) 
- Glide (Display image)
- Material Chip View (Pokemon type UI)
- Material SearchBar Android (Main page search bar)
- Swipe Refresh (Refresh pokemon list)

## App Description
In this app you can:
- Sort the list with names (ascending or descending order)
- Mark your favourite pokemon
- View the pokemon details in separate page

### Pokemon List
First time installation - the pokemon list will be downloaded from the internet. 
Subsequent opening the app (after closing them), the pokemon list will be extracted from the local db instead.

### Filter Pop Up Dialog
This is the pop up dialog that user can use for sorting and view favourite list of pokemon.
Tick the desired checkbox and click `Apply Filters` to apply the changes.
To reset back to default list, just open the pop up dialog again. This time chooses nothing and click `Apply Filters`.

### Pull-to-Refresh
Pull it down to refresh the pokemon list (which is downloaded from the internet. So, make sure there's internet connection).

## Maintainers
This project is mantained by:
* [Kristin Yen](https://gitlab.com/pinkies_26k)
