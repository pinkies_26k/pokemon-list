package com.ky.pokelistapp;


import com.ky.pokelistapp.Model.Pokedex;
import com.ky.pokelistapp.Retrofit.IPokemonDex;
import com.ky.pokelistapp.Retrofit.RetrofitClient;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RetrofitCallTest {

    @Test
    public void testApiResponse() {
        Retrofit retrofit = RetrofitClient.getInstance();
        IPokemonDex iPokemonDex = retrofit.create(IPokemonDex.class);
        CountDownLatch latch = new CountDownLatch(1);
        Call<Pokedex> call = iPokemonDex.getListPokemonByCall();


        call.enqueue(new Callback<Pokedex>() {
            @Override
            public void onResponse(Call call, Response response) {
                System.out.println("Success");
                latch.countDown();// notify the count down latch
                // assertEquals(..
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                System.out.println("Failure");
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
