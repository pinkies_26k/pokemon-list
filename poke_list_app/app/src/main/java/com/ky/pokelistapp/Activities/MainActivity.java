package com.ky.pokelistapp.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.ky.pokelistapp.Constant;
import com.ky.pokelistapp.Db.DbHelper;
import com.ky.pokelistapp.R;
import com.ky.pokelistapp.Utils.Common;
import com.ky.pokelistapp.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    DbHelper dbHelper;

    BroadcastReceiver showDetail = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equalsIgnoreCase(Common.KEY_ENABLE_HOME)){
                // Enable back the back button on toolbar
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);

                replaceFragment(intent, intent.getAction());
            }
        }
    };

    BroadcastReceiver showEvolution = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equalsIgnoreCase(Common.KEY_NUM_EVOLUTION)){

                replaceFragment(intent, intent.getAction());

            }
        }
    };

    private void replaceFragment(Intent intent, String key){

        Fragment detailFragment = PokemonDetailFragment.getInstance();
        String num = intent.getStringExtra(Constant.KEY_NUM);
        Bundle bundle = new Bundle();
        bundle.putString(Constant.KEY_NUM, num);
        detailFragment.setArguments(bundle);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if(key.equalsIgnoreCase(Common.KEY_NUM_EVOLUTION)){
            fragmentTransaction.remove(detailFragment);
        }
        fragmentTransaction.replace(R.id.fragment_list_pokemon, detailFragment);
        fragmentTransaction.addToBackStack("detail");
        fragmentTransaction.commit();

        // Set Pokemon name on toolbar
        binding.toolbar.setTitle(dbHelper.getPokemonName(num));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        dbHelper = new DbHelper(this);

        binding.toolbar.setTitle(getString(R.string.pokemon_list));
        setSupportActionBar(binding.toolbar);

        // Register broadcast
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(showDetail, new IntentFilter(Common.KEY_ENABLE_HOME));

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(showEvolution, new IntentFilter(Common.KEY_NUM_EVOLUTION));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                binding.toolbar.setTitle(getString(R.string.pokemon_list));

                // clear all fragment details and pop to list fragment
                getSupportFragmentManager().popBackStack("detail", FragmentManager.POP_BACK_STACK_INCLUSIVE);

                // Disable the back button on toolbar
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                break;
            default: break;
        }

        return super.onOptionsItemSelected(item);
    }
}