package com.ky.pokelistapp;

public class Constant {

    /*
     *  Key for bundle //TODO
     */
    public static final String KEY_NUM = "num";
    public static final String KEY_POSITION = "position";

    public static final int FAVOURITE_YES = 1;
    public static final int FAVOURITE_DEFAULT = 0;

    public static final long MIN_CLICK_INTERVAL = 1000;
}
