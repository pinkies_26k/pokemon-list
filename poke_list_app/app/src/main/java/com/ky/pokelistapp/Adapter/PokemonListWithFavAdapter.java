package com.ky.pokelistapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ky.pokelistapp.Constant;
import com.ky.pokelistapp.Interface.IItemClickListener;
import com.ky.pokelistapp.Model.Pokemon;
import com.ky.pokelistapp.R;
import com.ky.pokelistapp.Utils.Common;
import com.ky.pokelistapp.databinding.ItemPokeListHorizontalBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PokemonListWithFavAdapter extends RecyclerView.Adapter<PokemonListWithFavAdapter.MyViewHolder>
        implements Filterable {


    private List<Pokemon> pokemonList, originalList;
    private OnStateChangeListener mOnStateChangeListener;
    private Context mContext;
    private long mLastClickTime = 0;

    public PokemonListWithFavAdapter(Context context, List<Pokemon> pokemonList, OnStateChangeListener onStateChangeListener) {
        this.pokemonList = pokemonList;
        this.mContext = context;
        mOnStateChangeListener = onStateChangeListener;
        originalList = pokemonList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PokemonListWithFavAdapter.MyViewHolder(ItemPokeListHorizontalBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Pokemon pokemon = pokemonList.get(position);

        // Load Image
        Glide.with(mContext).load(pokemon.getImg()).into(holder.binding.ivPokemonImage);

        holder.binding.tvName.setText(pokemon.getName());
        holder.binding.tvNum.setText("#"+pokemon.getNum());

        PokemonTypeAdapter pAdapter = new PokemonTypeAdapter(mContext, pokemon.getType());
        holder.binding.rvType.setAdapter(pAdapter);

        holder.setItemClickListener((view, position12) -> {
            Common.hideKeyboardFrom(mContext, view);
            LocalBroadcastManager.getInstance(mContext)
                    .sendBroadcast(new Intent(Common.KEY_ENABLE_HOME).putExtra(Constant.KEY_NUM, pokemon.getNum()));
        });

        holder.binding.toggleFav.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if (isChecked) {
                holder.binding.toggleFav.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_star_filled));

            }else {
                holder.binding.toggleFav.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_star_not_filled));

            }
        });



        holder.binding.toggleFav.setOnClickListener(view -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < Constant.MIN_CLICK_INTERVAL) {
                return;
            }

            pokemon.setFavourite(holder.binding.toggleFav.isChecked() ? Constant.FAVOURITE_YES : Constant.FAVOURITE_DEFAULT);
            mOnStateChangeListener.onToggleStateChange(pokemon);
            mLastClickTime = SystemClock.elapsedRealtime();
        });

        holder.binding.toggleFav.setChecked(pokemon.getFavourite() == Constant.FAVOURITE_YES);


    }

    @Override
    public int getItemCount() {
        return pokemonList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    pokemonList = originalList;
                } else {
                    List<Pokemon> filteredList = new ArrayList<>();
                    for (Pokemon row : originalList) {

                        if (Pattern.compile(Pattern.quote(charString), Pattern.CASE_INSENSITIVE)
                                .matcher(row.getName()).find()) {
                            filteredList.add(row);
                        }
                    }

                    pokemonList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = pokemonList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                pokemonList = (ArrayList<Pokemon>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ItemPokeListHorizontalBinding binding;
        IItemClickListener iItemClickListener;

        public void setItemClickListener(IItemClickListener iItemClickListener){
            this.iItemClickListener = iItemClickListener;
        }

        private MyViewHolder(ItemPokeListHorizontalBinding itemPokemonListBinding){
            super(itemPokemonListBinding.getRoot());
            this.binding = itemPokemonListBinding;
            binding.ivPokemonImage.setOnClickListener(this);
            binding.toggleFav.setChecked(false);
            binding.toggleFav.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_star_not_filled));
            binding.rvType.setHasFixedSize(true);
            binding.rvType.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        }


        @Override
        public void onClick(View view) {
            iItemClickListener.onClick(view, getAdapterPosition());
        }
    }

    public interface OnStateChangeListener{
        void onToggleStateChange(Pokemon selectedPokemon);
    }
}
