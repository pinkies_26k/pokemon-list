package com.ky.pokelistapp.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.ky.pokelistapp.Model.CustomFilter;
import com.ky.pokelistapp.R;
import com.ky.pokelistapp.databinding.CustomDialogFilterBinding;

public class CustomFilterDialog implements
        View.OnClickListener {

    protected AlertDialog mAlertDialog;
    private CustomDialogListener mListener;
    protected boolean isShowing = false; // true when the alert dialog generation method is called
    private Context mContext;
    private CustomDialogFilterBinding binding;
    private CustomFilter customFilter;

    public CustomFilterDialog(Context context, CustomDialogListener listener) {
        mContext = context;
        this.mListener = listener;
    }

    public void showDialog(){
        isShowing = true;
        binding = CustomDialogFilterBinding.inflate(LayoutInflater.from(mContext));

        customFilter = new CustomFilter();
        setViewListeners();

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(binding.getRoot());
        builder.setCancelable(false);
        mAlertDialog = builder.create();
        mAlertDialog.show();
    }

    private void setViewListeners() {
//        binding.cbFavourites.setOnCheckedChangeListener(this);
//        binding.cbSortByName.setOnCheckedChangeListener(this);
//        binding.rgOrder.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
//        {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                // checkedId is the RadioButton selected
//                switch(checkedId){
//                    case R.id.rb_ascending:
//                        customFilter.setAscending();
//                        break;
//                    case R.id.rb_descending:
//                        break;
//                }
//            }
//        });

        binding.btnApplyFilters.setOnClickListener(this);
        binding.btnCancel.setOnClickListener(this);
        binding.cbSortByName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                for(int i = 0; i < binding.rgOrder.getChildCount(); i++){
                    ((RadioButton)binding.rgOrder.getChildAt(i)).setEnabled(b);
                }
            }
        });

        //set default to false
        for(int i = 0; i < binding.rgOrder.getChildCount(); i++){
            ((RadioButton)binding.rgOrder.getChildAt(i)).setEnabled(false);
        }

        View view = binding.getRoot();
        view.jumpDrawablesToCurrentState();

    }

//    @Override
//    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//        switch(compoundButton.getId()){
//            case R.id.cb_favourites:
//                customFilter.setFavorites(b);
//                break;
//            case R.id.cb_sort_by_name:
//                customFilter.setSortBy(b);
//                break;
//        }
//    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_apply_filters){

            CustomFilter customFilter = new CustomFilter();
            customFilter.setSortBy(binding.cbSortByName.isChecked());
            customFilter.setFavorites(binding.cbFavourites.isChecked());
            customFilter.setSortByTypeCheckedRbId(binding.rgOrder.getCheckedRadioButtonId());
            mListener.onApplyFiltersClick(customFilter);
        }else if(view.getId() == R.id.btn_cancel){
            dismiss();
        }
    }

    public void dismiss() {
        isShowing = false;

        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }

    public interface CustomDialogListener {
        void onApplyFiltersClick(CustomFilter customFilter);
    }
}
