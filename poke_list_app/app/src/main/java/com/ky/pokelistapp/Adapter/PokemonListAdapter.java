package com.ky.pokelistapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ky.pokelistapp.Constant;
import com.ky.pokelistapp.Interface.IItemClickListener;
import com.ky.pokelistapp.Model.Pokemon;
import com.ky.pokelistapp.Utils.Common;
import com.ky.pokelistapp.databinding.ItemPokemonListBinding;

import java.lang.ref.WeakReference;
import java.util.List;

@Deprecated
public class PokemonListAdapter extends RecyclerView.Adapter<PokemonListAdapter.MyViewHolder> {

    private List<Pokemon> pokemonList;
    private OnItemClickListener mListerner;
    private Context mContext;
    private long mLastClickTime = 0;

    public PokemonListAdapter(Context context, List<Pokemon> pokemonList) {
        this.pokemonList = pokemonList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(ItemPokemonListBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final Pokemon pokemon = pokemonList.get(position);

        // Load Image
        Glide.with(mContext).load(pokemon.getImg()).into(holder.binding.ivPokemonImage);
        // Set Name
        holder.binding.tvPokemonName.setText(pokemon.getName());

        // Event
        holder.setItemClickListener((view, position1) -> {
            Common.hideKeyboardFrom(mContext, view);
            LocalBroadcastManager.getInstance(mContext)
            .sendBroadcast(new Intent(Common.KEY_ENABLE_HOME).putExtra(Constant.KEY_NUM, pokemon.getNum()));

        });
    }

    @Override
    public int getItemCount() {
        return pokemonList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ItemPokemonListBinding binding;
        private WeakReference<OnItemClickListener> listenerRef;

        IItemClickListener iItemClickListener;
        //TODO
        public void setItemClickListener(IItemClickListener iItemClickListener){
            this.iItemClickListener = iItemClickListener;
        }

        private MyViewHolder(ItemPokemonListBinding itemPokemonListBinding){
            super(itemPokemonListBinding.getRoot());
            this.binding = itemPokemonListBinding;

//            mContext = itemPokemonListBinding.getRoot().getContext();
            listenerRef = new WeakReference<>(mListerner);
            binding.ivPokemonImage.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            iItemClickListener.onClick(view, getAdapterPosition());
        }
    }

    public interface OnItemClickListener{
        void onItemSelected(Pokemon selectedPokemon);
    }
}
