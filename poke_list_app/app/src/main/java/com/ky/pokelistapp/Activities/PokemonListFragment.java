package com.ky.pokelistapp.Activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.ky.pokelistapp.Adapter.PokemonListWithFavAdapter;
import com.ky.pokelistapp.Db.DbHelper;
import com.ky.pokelistapp.Model.CustomFilter;
import com.ky.pokelistapp.Model.Pokemon;
import com.ky.pokelistapp.R;
import com.ky.pokelistapp.Retrofit.IPokemonDex;
import com.ky.pokelistapp.Retrofit.RetrofitClient;
import com.ky.pokelistapp.Utils.Common;
import com.ky.pokelistapp.Utils.CustomFilterDialog;
import com.ky.pokelistapp.Utils.ItemOffSetDecoration;
import com.ky.pokelistapp.databinding.FragmentPokemonListBinding;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class PokemonListFragment extends Fragment implements PokemonListWithFavAdapter.OnStateChangeListener,
        CustomFilterDialog.CustomDialogListener {

    private static final String TAG = PokemonListFragment.class.getSimpleName();

    FragmentPokemonListBinding binding;
    IPokemonDex iPokemonDex;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    DbHelper dbHelper;
    CustomFilterDialog customFilterDialog;

    static PokemonListFragment instance;
    public Context mContext;
    private PokemonListWithFavAdapter defaultPokemonListAdapter;
    List<String> lastSuggestedList = new ArrayList<>();

    public static PokemonListFragment getInstance(){
        if(instance == null)
            instance = new PokemonListFragment();
        return instance;
    }

    public PokemonListFragment(){
        Retrofit retrofit = RetrofitClient.getInstance();
        iPokemonDex = retrofit.create(IPokemonDex.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity){

            mContext = context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DbHelper(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentPokemonListBinding.inflate(inflater, container, false);

        setupViews();
        fetchData();

        return binding.getRoot();
    }

    private void setupViews(){
        binding.rvPokemonList.setHasFixedSize(true);
        binding.rvPokemonList.setLayoutManager(new LinearLayoutManager(mContext));
        ItemOffSetDecoration itemOffSetDecoration = new ItemOffSetDecoration(mContext, R.dimen.extra_small_width);
        binding.rvPokemonList.addItemDecoration(itemOffSetDecoration);

        binding.searchBar.setCardViewElevation(10);
        binding.searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String searchKeyword = binding.searchBar.getText().toLowerCase().trim();

                List<String> newSuggestedList = new ArrayList<>();
                for(String search: lastSuggestedList){
                    if(search.toLowerCase().contains(searchKeyword)){
                        newSuggestedList.add(search);
                    }
                }

                binding.searchBar.setLastSuggestions(newSuggestedList);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.searchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                if(!enabled){
                    defaultPokemonListAdapter.getFilter().filter("");
                    binding.rlFilter.setVisibility(View.VISIBLE);
                }else{
                    binding.rlFilter.setVisibility(View.GONE);
                }

            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                defaultPokemonListAdapter.getFilter().filter(text.toString());
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });

        binding.srlPullToRefresh.setOnRefreshListener(() -> {
            swipeToGetDataFromInternet();
            binding.srlPullToRefresh.setRefreshing(false);
        });

        binding.btnFilter.setOnClickListener(view -> setupCustomFilterDialog());
    }

    private void setupCustomFilterDialog(){
        customFilterDialog  = new CustomFilterDialog(mContext, this);
        customFilterDialog.showDialog();
    }



    private void fetchData(){

        List<Pokemon> pokemonList = dbHelper.getPokemonList();


        if(pokemonList.size() == 0){

            // Check internet connection
            if(!Common.isNetworkConnected(mContext)){
                Toast.makeText(mContext, getString(R.string.no_internet_available), Toast.LENGTH_LONG).show();
                binding.tvPokemonListNa.setVisibility(View.VISIBLE);

            }else {

                compositeDisposable.add(iPokemonDex.getListPokemon()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(pokedex -> {

                            dbHelper.clearData();
                            dbHelper.insertPokemon(pokedex.getPokemon());
                            Common.commonPokemonList = dbHelper.getPokemonList();

                            defaultPokemonListAdapter = new PokemonListWithFavAdapter(mContext, Common.commonPokemonList, this);
                            binding.rvPokemonList.setAdapter(defaultPokemonListAdapter);

                            lastSuggestedList.clear();
                            for (Pokemon pokemon : Common.commonPokemonList) {
                                lastSuggestedList.add(pokemon.getName());
                            }

                            binding.searchBar.setLastSuggestions(lastSuggestedList);

                        }));

            }
        }else {

            Common.commonPokemonList = pokemonList;
            defaultPokemonListAdapter = new PokemonListWithFavAdapter(mContext, Common.commonPokemonList, this);
            binding.rvPokemonList.setAdapter(defaultPokemonListAdapter);

            lastSuggestedList.clear();
            for (Pokemon pokemon : Common.commonPokemonList) {
                lastSuggestedList.add(pokemon.getName());
            }

            binding.searchBar.setLastSuggestions(lastSuggestedList);
        }
    }

    private void swipeToGetDataFromInternet(){
        // Check internet connection
        if(!Common.isNetworkConnected(mContext)){
            Toast.makeText(mContext, getString(R.string.no_internet_available), Toast.LENGTH_LONG).show();

        }else {
            compositeDisposable.add(iPokemonDex.getListPokemon()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(pokedex -> {

                        dbHelper.clearData();
                        dbHelper.insertPokemon(pokedex.getPokemon());
                        Common.commonPokemonList.clear();
                        List<Pokemon> newPokemonList = dbHelper.getPokemonList();
                        Common.commonPokemonList.addAll(newPokemonList);
                        defaultPokemonListAdapter.notifyDataSetChanged();

                        lastSuggestedList.clear();
                        for (Pokemon pokemon : Common.commonPokemonList) {
                            lastSuggestedList.add(pokemon.getName());
                        }

                        binding.searchBar.setLastSuggestions(lastSuggestedList);

                    }));
        }
    }

    @Override
    public void onToggleStateChange(Pokemon selectedPokemon) {
        dbHelper.updatePokemonFav(selectedPokemon.getId(), selectedPokemon.getFavourite());
    }

    @Override
    public void onApplyFiltersClick(CustomFilter customFilter) {

        List<Pokemon> newPokemonList = dbHelper.getPokemonList(customFilter);

        Common.commonPokemonList.clear();
        Common.commonPokemonList.addAll(newPokemonList);

        defaultPokemonListAdapter.notifyDataSetChanged();

        customFilterDialog.dismiss();
    }
}