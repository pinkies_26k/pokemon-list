package com.ky.pokelistapp.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ky.pokelistapp.Constant;
import com.ky.pokelistapp.Model.CustomFilter;
import com.ky.pokelistapp.Model.EvolSimple;
import com.ky.pokelistapp.Model.Pokemon;
import com.ky.pokelistapp.R;
import com.ky.pokelistapp.Utils.Common;

import java.util.ArrayList;
import java.util.List;

public class DbHelper extends SQLiteOpenHelper {

    /* Database update record :
     * v1.0.0   -- DATABASE_VERSION = 1
     */

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "poke_clone";

    private static final String POKEMON_TABLE = "tb_pokemon";
    private static final String POKEMON_TYPE_TABLE = "tb_pokemon_type";
    private static final String WEAKNESSES_TABLE = "tb_weaknesses";

    private static final String CREATES_POKEMON_TABLE = "CREATE TABLE IF NOT EXISTS " + POKEMON_TABLE + "("+
            "id INTEGER, num TEXT, name TEXT, img TEXT, type TEXT, height TEXT, weight TEXT, candy TEXT, candy_count INTEGER, egg TEXT, " +
            "weaknesses TEXT, prev_evolution TEXT DEFAULT '', next_evolution TEXT DEFAULT '', favourite INTEGER DEFAULT "+ Constant.FAVOURITE_DEFAULT+")";
    private static final String CREATES_POKEMON_TYPE_TABLE = "CREATE TABLE IF NOT EXISTS " + POKEMON_TYPE_TABLE + "("+
            "pokemon_id INTEGER, type TEXT )";
    private static final String CREATES_WEAKNESSES_TABLE = "CREATE TABLE IF NOT EXISTS " + WEAKNESSES_TABLE + "("+
            "pokemon_id INTEGER, weaknesses TEXT )";

    private static final String TAG = DbHelper.class.getSimpleName();

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // create table
        db.execSQL(CREATES_POKEMON_TABLE);
        db.execSQL(CREATES_POKEMON_TYPE_TABLE);
        db.execSQL(CREATES_WEAKNESSES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "oldVersion: "+ oldVersion);
        Log.i(TAG, "newVersion: "+ newVersion);

        // Create tables again
        onCreate(db);
    }

    /**
     * Clear data in tables before inserting new data
     */
    public void clearData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        try
        {
            db.execSQL("DELETE FROM " + POKEMON_TABLE );
            db.execSQL("DELETE FROM " + POKEMON_TYPE_TABLE );
            db.execSQL("DELETE FROM " + WEAKNESSES_TABLE );

            db.setTransactionSuccessful();
        }
        catch (Exception e)
        {
            Log.e(TAG, Common.getStackTrace(e));
        }
        finally
        {
            db.endTransaction();
            db.close();
        }
    }

    public void insertPokemon(List<Pokemon> pokemonList)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        try
        {
            for (Pokemon obj:pokemonList)
            {
                ContentValues values = new ContentValues();

                values.put("id",obj.getId());
                values.put("num",obj.getNum());
                values.put("name",obj.getName());
                values.put("img",obj.getImg());
                values.put("height",obj.getHeight());
                values.put("weight",obj.getWeight());
                values.put("candy",obj.getCandy());
                values.put("candy_count",obj.getCandy_count());
                values.put("egg",obj.getEgg());

                /*
                 * Only need to store ONE pokemon num of the previous evolution
                 * Same goes to next evolution
                 */
                if (!Common.isObjectListNullOrEmpty(obj.getPrev_evolution())){
                    int index = obj.getPrev_evolution().size() - 1;
                    values.put("prev_evolution",obj.getPrev_evolution().get(index).getNum());
                }

                if (!Common.isObjectListNullOrEmpty(obj.getNext_evolution())){

                    values.put("next_evolution",obj.getNext_evolution().get(0).getNum());
                }

                for(String type: obj.getType()){
                    ContentValues values2 = new ContentValues();
                    values2.put("pokemon_id", obj.getId());
                    values2.put("type", type);
                    db.insert(POKEMON_TYPE_TABLE, null, values2);
                }

                for(String weakness: obj.getWeaknesses()){
                    ContentValues values3 = new ContentValues();
                    values3.put("pokemon_id", obj.getId());
                    values3.put("weaknesses", weakness);
                    db.insert(WEAKNESSES_TABLE, null, values3);
                }

                db.insert(POKEMON_TABLE, null, values);
            }
            db.setTransactionSuccessful();
        }
        catch (Exception e)
        {
            Log.e(TAG, Common.getStackTrace(e));
        }
        finally
        {
            db.endTransaction();
            db.close();
        }
    }

    public List<Pokemon> getPokemonList(){
        return getPokemonList(null);
    }

    public List<Pokemon> getPokemonList(CustomFilter customFilter){
        List<Pokemon> pokemonList = new ArrayList<>();

        String selectQuery = "SELECT * FROM "+ POKEMON_TABLE;

        /*
         * If there is info in the custom filter, the query is required for modification
         * based on the user selection
         */
        if(customFilter != null){

            int favouriteFlag = customFilter.isFavorites() ? Constant.FAVOURITE_YES : Constant.FAVOURITE_DEFAULT;
            if(customFilter.isFavorites()) {
                selectQuery = selectQuery.concat(" WHERE favourite = " + favouriteFlag);
            }
            if(customFilter.isSortBy()){
                String sortByType = customFilter.getSortByTypeCheckedRbId() == R.id.rb_ascending ?  "ASC" : "DESC";
                selectQuery = selectQuery.concat(" ORDER BY name "+ sortByType);
            }
        }

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null){
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                do {
                    Pokemon pokemon = new Pokemon(
                            cursor.getInt(cursor.getColumnIndex("id" )),
                            cursor.getString(cursor.getColumnIndex("num")),
                            cursor.getString(cursor.getColumnIndex("name")),
                            cursor.getString(cursor.getColumnIndex("img" )),
                            cursor.getString(cursor.getColumnIndex("height")),
                            cursor.getString(cursor.getColumnIndex("weight")),
                            cursor.getString(cursor.getColumnIndex("candy" )),
                            cursor.getInt(cursor.getColumnIndex("candy_count" )),
                            cursor.getString(cursor.getColumnIndex("egg")),
                            cursor.getString(cursor.getColumnIndex("prev_evolution")),
                            cursor.getString(cursor.getColumnIndex("next_evolution" )),
                            cursor.getInt(cursor.getColumnIndex("favourite" ))
                    );

                    pokemon.setType(getTypeList(pokemon.getId()));
                    pokemon.setWeaknesses(getWeaknessesList(pokemon.getId()));

                    pokemonList.add(pokemon);


                } while (cursor.moveToNext());
            }

            // close db connection
            cursor.close();
        }
        db.close();

        return pokemonList;
    }

    public List<String> getTypeList(int pokemon_id){
        List<String> typeList = new ArrayList<>();

        String selectQuery = "SELECT * FROM "+ POKEMON_TYPE_TABLE +
                " WHERE pokemon_id = "+pokemon_id;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null){
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                do {
                    typeList.add(cursor.getString(cursor.getColumnIndex("type")));

                } while (cursor.moveToNext());
            }

            // close db connection
            cursor.close();
        }
        db.close();

        return typeList;
    }

    public List<String> getWeaknessesList(int pokemon_id){
        List<String> weaknessesList = new ArrayList<>();

        String selectQuery = "SELECT * FROM "+ WEAKNESSES_TABLE +
                " WHERE pokemon_id = "+pokemon_id;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null){
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                do {
                    weaknessesList.add(cursor.getString(cursor.getColumnIndex("weaknesses")));

                } while (cursor.moveToNext());
            }

            // close db connection
            cursor.close();
        }
        db.close();

        return weaknessesList;
    }

    public void updatePokemonFav(int pokemon_id, int fav_flag) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("favourite", fav_flag);

        // updating row
        db.update(POKEMON_TABLE, values, "id" + " = ?",
                new String[]{String.valueOf(pokemon_id)});

        db.close();
    }

    public EvolSimple getEvol(String num){

        EvolSimple evolSimple = null;

        String selectQuery = "SELECT * FROM "+ POKEMON_TABLE +
                " WHERE num = '"+num+"'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null){
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                evolSimple = new EvolSimple(
                        cursor.getString(cursor.getColumnIndex("num")),
                        cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getString(cursor.getColumnIndex("img"))
                        );


            }

            // close db connection
            cursor.close();
        }
        db.close();

        return evolSimple;
    }

    public String getPokemonName(String pokemon_num){
        String name = "";

        String selectQuery = "SELECT name FROM "+ POKEMON_TABLE +
                " WHERE num = '"+pokemon_num+"'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null){
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                    name = cursor.getString(cursor.getColumnIndex("name"));
            }

            // close db connection
            cursor.close();
        }
        db.close();

        return name;
    }

    public Pokemon getPokemon(String pokemon_num){
        Pokemon pokemon = null;

        String selectQuery = "SELECT * FROM "+ POKEMON_TABLE +
                " WHERE num = '"+pokemon_num+"'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null){
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                    pokemon = new Pokemon(
                            cursor.getInt(cursor.getColumnIndex("id" )),
                            cursor.getString(cursor.getColumnIndex("num")),
                            cursor.getString(cursor.getColumnIndex("name")),
                            cursor.getString(cursor.getColumnIndex("img" )),
                            cursor.getString(cursor.getColumnIndex("height")),
                            cursor.getString(cursor.getColumnIndex("weight")),
                            cursor.getString(cursor.getColumnIndex("candy" )),
                            cursor.getInt(cursor.getColumnIndex("candy_count" )),
                            cursor.getString(cursor.getColumnIndex("egg")),
                            cursor.getString(cursor.getColumnIndex("prev_evolution")),
                            cursor.getString(cursor.getColumnIndex("next_evolution" )),
                            cursor.getInt(cursor.getColumnIndex("favourite" ))
                    );

                    pokemon.setType(getTypeList(pokemon.getId()));
                    pokemon.setWeaknesses(getWeaknessesList(pokemon.getId()));

            }

            // close db connection
            cursor.close();
        }
        db.close();

        return pokemon;
    }
}
