package com.ky.pokelistapp.Model;

public class CustomFilter {
    private boolean favorites;
    private boolean sortBy;

    private int sortByTypeCheckedRbId = -1;

    public boolean isFavorites() {
        return favorites;
    }

    public void setFavorites(boolean favorites) {
        this.favorites = favorites;
    }

    public boolean isSortBy() {
        return sortBy;
    }

    public void setSortBy(boolean sortBy) {
        this.sortBy = sortBy;
    }

    /**
     *
     * @return checked radio button ID
     */
    public int getSortByTypeCheckedRbId() {
        return sortByTypeCheckedRbId;
    }

    public void setSortByTypeCheckedRbId(int sortByTypeCheckedRbId) {
        this.sortByTypeCheckedRbId = sortByTypeCheckedRbId;
    }
}
