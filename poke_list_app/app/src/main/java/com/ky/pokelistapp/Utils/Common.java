package com.ky.pokelistapp.Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.ky.pokelistapp.Model.Evolution;
import com.ky.pokelistapp.Model.Pokemon;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Note: Some functions & declarations are from https://github.com/eddydn/PokemonCommon/blob/master/Common.java
 */
public class Common {
    public static final String KEY_ENABLE_HOME = "enable_home";
    public static final String KEY_NUM_EVOLUTION = "num_evolution";
    public static List<Pokemon> commonPokemonList = new ArrayList<>();

    /**
     *
     * @param type (pokemon type)
     * @return color int
     */
    public static int getColorByType(String type) {
        switch(type)
        {

            case "Normal":
                return Color.parseColor("#A4A27A");

            case "Dragon":
                return Color.parseColor("#743BFB");

            case "Psychic":
                return Color.parseColor("#F15B85");

            case "Electric":
                return Color.parseColor("#E9CA3C");

            case "Ground":
                return Color.parseColor("#D9BF6C");

            case "Grass":
                return Color.parseColor("#81C85B");

            case "Poison":
                return Color.parseColor("#A441A3");

            case "Steel":
                return Color.parseColor("#BAB7D2");

            case "Fairy":
                return Color.parseColor("#DDA2DF");

            case "Fire":
                return Color.parseColor("#F48130");

            case "Fight":
                return Color.parseColor("#BE3027");

            case "Bug":
                return Color.parseColor("#A8B822");

            case "Ghost":
                return Color.parseColor("#705693");

            case "Dark":
                return Color.parseColor("#745945");

            case "Ice":
                return Color.parseColor("#9BD8D8");

            case "Water":
                return Color.parseColor("#658FF1");

            default:
                return Color.parseColor("#658FA0");
        }
    }

    public static Pokemon findPokemonByNum(String num){
        for(Pokemon pokemon: Common.commonPokemonList){
            if(pokemon.getNum().equals(num)) return pokemon;
        }

        return null;
    }

    /**
     *
     * @param context
     * @return boolean (true if network is connected)
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    /**
     * Print out details error
     * @param e
     * @return
     */
    public static String getStackTrace(Exception e) {

        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));

        return errors.toString();

    }

    /**
     * Check if String List is Empty or Null
     * @param stringList
     * @return
     */
    public static boolean isObjectListNullOrEmpty(List<Evolution> stringList) {
        return stringList == null || stringList.isEmpty();
    }

    /**
     * Hide Keyboard
     * @param context
     * @param view
     */
    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
